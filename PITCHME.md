
## Systemadministration
#### Übung / vLAB

---
## Einführung

---
## SAM Übung / vLAB

* Vorstellrunde
* Vorraussetzungen
* Ziele
* Housekeeping
* Benotung
* Tools

---
## Vorstellrunde

* Mein Name Roland Stumpner
* seit zirka 20 Jahren in der IT
* seit zirka 10 Jahren an der FH-OOE IT
* Aktuell im Bereich Netzwerk und Security
* Lieblingssprache: Python
* Essen: Speckjause
* Freizeit: Bergsteigen

---
## Vorraussetzungen
* Grundkenntnisse in der Funktionsweise von Computern
* Grundkenntnisse in Betriebsystemen (Linux und Windows)
* Grundkenntnisse im Bereich Netzwerktechnik

---
## SAM Übungen Ziele
* Grundkenntnisse für den Betrieb von IT Infrastruktur (Rechenzentrum)
* Moderne Methoden zur Installation und Betrieb einer Applikationsinfrastrkutur (Devops/Microservices)
* Planung und Dokumentation von Systemen und Diensten
* Herstellen einer Übungsumgebung zum Trainieren der notwendigen Fertigkeiten 

---
## Was machen wir nicht!
* Step by Step Anleitungen für Installation oder Konfiguration von Systemen oder Diensten
* Wie Betriebssysteme funktionieren oder diese Bedient werden
* Wie Programme oder Scripts geschrieben werden
* Kryptographische Grundlagen

---
## Houskeeping
* Onlineplattform Moodle
* Fragen bitte gleich
* Belohnung fürs Mitdenken
* Nicht vergessen Evaluierung in LEVIS

---
## Organisatorisches 
#### Benotung (Übung)

* Benotet wird die Dokumentation
* Abgabe bitte im Moodle als PDF
* Abgabe zirka 2 Wochen nach der Übung
* Benotung nach 2 Wochen

---
## Dokumentation

* Grundlage ist die Nachvollzieharkeit der Dokumentation für Kollegen
* Enthalten sein müssen Ablaufbeschreibung , Screenshoots und Konsolelogs
* Besser sind zusätzlich Inhaltsangabe , Beschreibung von Fehlern und Lösungen 
* Quellen und Links

---
## Tooltipp

* Texteditor ATOM oder Visualstudio Code
* Markdown oder LATEX
* Screenshots mit Greenshot
* Sourcecontroll mit git

---
## Alternativen
* Dokuwiki 
* Onenote
* Hugoio
  
---
## Anmerkungen
* keine Vorlage für Dokumentation
* Kritik und Verbesserungsvorschläge offen und sofort
* Empfohlen ist immer Ubuntu LTS (18.04)

---
## Fernlehre

* Die Einführung Live zu den Terminen der Fernlehre (Präsenz in MS Teams)
* Aufgaben zu Hause (zeitsouverän)
* Fragen per Forum (zeitsouverän)

---
## Webminar Knigge
* zu den Fernlehre Terminen im Kalender
* pünktlich
* 30 Minuten Content dann Fragen
* Fragen im Chat
* einer Spricht
* Aussprechen lassen
* Mikrofon Stummschalten wenn nicht gesprochen wird

---
## Weiterführendes
* NRE Labs (Basis Linux Übungen)
  * https://nrelabs.io/

---
## Links
* ATOM (https://atom.io/)
* Visual Studio Code (https://code.visualstudio.com/)
* Markdown (https://de.wikipedia.org/wiki/Markdown)
* LATEX (https://de.wikipedia.org/wiki/LaTeX)
* Greenshot (https://getgreenshot.org/)
* git (https://git-scm.com/)
* Gitlab (https://about.gitlab.com/)
* Dokuwiki (https://www.dokuwiki.org/dokuwiki)
* Static Site Generator Hugo ( https://gohugo.io/)